:

ORIG='FONT COLOR="#000000"'
DEST='FONT COLOR="#DEDEDE"'
sed -i "s/$ORIG/$DEST/g" /usr/lib/mailman/templates/*/*.html

BGORIG='BGCOLOR="#ffffff"'
BGDEST='BGCOLOR="#1A1A1A"'
sed -i "s/$BGORIG/$BGDEST/gi" /usr/lib/mailman/templates/*/*.html

BGORIG='BGCOLOR="#cccccc"'
BGDEST='BGCOLOR="#4A4A4A"'
sed -i "s/$BGORIG/$BGDEST/gi" /usr/lib/mailman/templates/*/*.html

BGORIG='BGCOLOR="#fff0d0"'
BGDEST='BGCOLOR="#4F4020"'
sed -i "s/$BGORIG/$BGDEST/gi" /usr/lib/mailman/templates/*/*.html

BGORIG='BGCOLOR="#dddddd"'
BGDEST='BGCOLOR="#555555"'
sed -i "s/$BGORIG/$BGDEST/gi" /usr/lib/mailman/templates/*/*.html

BGORIG='BGCOLOR="#99CCFF"'
BGDEST='BGCOLOR="#003377"'
sed -i "s/$BGORIG/$BGDEST/gi" /usr/lib/mailman/templates/*/*.html
