#!/bin/bash

# Convert regular files to symlinks in package archive
# Saves up some space when pkgpool is present in archive server
# nous 2024


source /usr/share/makepkg/util/message.sh
colorize
plain() {
    echo "${ALL_OFF}${1}"
}
bold() {
    echo "${BOLD}${1}${ALL_OFF}"
}
mark() {
    echo "${GREEN}${1}${ALL_OFF}"
}
input() {
    echo "${CYAN}${1}${ALL_OFF}"
}
warn() {
    echo "${MAGENTA}${1}${ALL_OFF}"
}
action() {
    echo "${YELLOW}${1}${ALL_OFF}"
}
error() {
    echo "${RED}${1}${ALL_OFF}"
}
nplain() {
    echo -n "${ALL_OFF}${1}"
}
nbold() {
    echo -n "${BOLD}${1}${ALL_OFF}"
}
nmark() {
    echo -n "${GREEN}${1}${ALL_OFF}"
}
ninput() {
    echo -n "${CYAN}${1}${ALL_OFF}"
}
nwarn() {
    echo -n "${MAGENTA}${1}${ALL_OFF}"
}
naction() {
    echo -n "${YELLOW}${1}${ALL_OFF}"
}
nerror() {
    echo -n "${RED}${1}${ALL_OFF}"
}
croak() {
    echo "${RED}${1}${ALL_OFF}"
    exit 1
}

PKGPOOL=/srv/pkgpool
ARCHIVE=/tmp/ARCHIVE
PACKAGES=$ARCHIVE/packages
REPOS=$ARCHIVE/repos
ARCH='os/x86_64'
SYMLINK="ln -svf"

echo "-----------------------------------"
nmark "Will operate on "
bold "$ARCHIVE"
error "Press ENTER to continue"
error "(and forever cry in darkness)"
error "or CTRL-C to abort"
echo "-----------------------------------"
read

DEBUG=YES
TEST=''
[ $TEST ] && SYMLINK="error"
# ext / before ext
# ${f##*.} / ${f%.*}

cd $REPOS || continue
for YEAR in *; do
  cd $YEAR || continue
  for MONTH in *; do
    cd $MONTH || continue
    for DAY in *; do
      cd $DAY || continue
      for REPO in *; do
        cd $REPO/$ARCH || continue
        PWD=$(pwd)
        [ $DEBUG ] && action "=== Entering $PWD"
# Delete repo.db.old etc
        rm -f ${REPO}.*.old
# Load up both pkg and .sigs, then get ugly
        for FILE in $(find -name "*.pkg.tar.*" -type f -printf '%P\n'); do
          PKG=${FILE%.sig}
          [ $DEBUG ] && bold ">>> Found $FILE"
          PKGFOUND='NO'
          SIGFOUND='NO'
# 1, check if files are regular and symlink instead
          [[ -f ${PKG} && -f ${PKGPOOL}/${PKG} ]] && { $SYMLINK ${PKGPOOL}/${PKG}; PKGFOUND='YES'; [ $DEBUG ] && mark "--> Found regular file $PKG, created symlink from ${PKGPOOL}/${PKG}"; }
          [[ -f ${PKG}.sig && -f ${PKGPOOL}/${PKG}.sig ]] && { $SYMLINK ${PKGPOOL}/${PKG}.sig; SIGFOUND='YES'; [ $DEBUG ] && mark "--> Found regular file ${PKG}.sig, created symlink from ${PKGPOOL}/${PKG}.sig"; }
          [[ $PKGFOUND == 'YES' && $SIGFOUND == 'YES' ]] && continue;
# 2, present pkg, missing .sig
          [[ -L $PKG && -f ${PKGPOOL}/${PKG}.sig && ! -L ${PKG}.sig ]] && { nerror "+++ Missing ${PKG}.sig: "; $SYMLINK ${PKGPOOL}/${PKG}.sig; }
# 3, present .sig, missing pkg
          [[ -L ${PKG}.sig && -f ${PKGPOOL}/${PKG} && ! -L ${PKG} ]] && { nerror "+++ Missing ${PKG}: "; $SYMLINK ${PKGPOOL}/${PKG}; }
# 4, catch-all, create missing symlinks if present in pkgpool - is this even needed?
#          [ $SIGFOUND == 'YES' ] && -f ${PKGPOOL}/${PKG} ]] && { error "--- $PWD: Missing ${PKG}"; $SYMLINK ${PKGPOOL}/${PKG}; continue; }
#          [ $PKGFOUND == 'YES' ] && -f ${PKGPOOL}/${PKG}.sig ]] && { error "--- $PWD: Missing ${PKG}.sig"; $SYMLINK ${PKGPOOL}/${PKG}.sig; continue; }
        done
        cd $REPOS/$YEAR/$MONTH/$DAY
      done
      cd $REPOS/$YEAR/$MONTH
    done
    cd $REPOS/$YEAR
  done
  cd $REPOS
done

# Delete any stale symlinks
find $ARCHIVE -xtype l -delete
