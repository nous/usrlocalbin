#!/bin/bash
# Create torrent files and magnets for our ISOs
# nous 2020-2023

# Needed for parameter expansions below
shopt -s extglob
shopt -s expand_aliases


cd /srv/torrents

# Remove broken symlinks
find . -xtype l -delete

# Remove stale .torrent
for f in $(ls *.torrent 2>/dev/null); do
#    [ -s `basename ${f} .torrent` ] || rm -f $f
    [ -s ${f//.torrent} ] || rm -f $f
done

# Remove stale .magnet
for f in $(ls *.magnet 2>/dev/null); do
#    [ -s `basename ${f} .magnet` ] || rm -f $f
    [ -s ${f//.magnet} ] || rm -f $f
done

# Remove stale .part
for f in $(ls *.part 2>/dev/null); do
#    [ -s `basename ${f} .part` ] || rm -f $f
#  echo "p: $f"
    t=${f//.part}
    torid=$(transmission-remote -l | awk "/$t/ {print \$1}")
    torid=${torid//\*}
    transmission-remote -t $torid -rad
    rm -f ${f//.part}
    rm -f $f $t.torrent $t.magnet
done

# Remove actual ISOs, only symlinks allowed
for f in $(ls *.iso 2>/dev/null); do
#    [ -s `basename ${f} .torrent` ] || rm -f $f
    if [ ! -L $f ]; then
        torid=$(transmission-remote -l | awk "/$f/ {print \$1}")
        torid=${torid//\*}
        transmission-remote -t $torid -rad
        rm -f $f $f.torrent $f.magnet
#        rm -f $f	# should be deleted by -rad
    fi
done

for f in $(ls *.iso 2>/dev/null); do
    torid=$(transmission-remote -l | awk "/$f/ {print \$1}")
    torid=$(echo $torid)	# remove non-printable chars
    torid=${torid//\*}		# remove asterisk
    first=${torid% *}
    second=${torid#* }
    [ $first == $second ] && unset second
    state=$(transmission-remote -t $first -i | awk '/State/ {print $2}')
# Verify and start non-Done torrents
    [[ $state == Stopped || $state == Unknown ]] && transmission-remote -t $first -v -s
# also search for and remove possible duplicates
    [[ $first -gt 0 && $second -gt 0 ]] && transmission-remote -t $second -r
done

# Remove old/stale torrents
transmission-remote -l | sed 's/ MB/_MB/;s/ GB/_GB/' | grep -v Verify | /usr/local/bin/torrents-clear.pl
for f in $(transmission-remote -l | sed 's/ MB/_MB/;s/ GB/_GB/' | grep Stopped | awk '/Done/ {print $9}'); do
    torid=$(transmission-remote -l | awk "/$f/ {print \$1}")
    torid=${torid//\*}
    transmission-remote -t $torid -r
done

# Remove broken symlinks
find . -xtype l -delete

# symlink all .iso from subdirs into a flat dir, remove all else
for f in $(ls /srv/torrents/{weekly-,}iso/*.iso 2>/dev/null); do
    iso=$(basename $f)
    # skip existing
    [ -L $iso ] && continue || ln -s $f
done

for f in $(ls *.iso 2>/dev/null); do
    [ -s ${f}.torrent ] && continue
#    echo "Not skipping $f"
    export CHANGED=true
    echo "CHANGED: $f"
    mktorrent -a http://tracker1.artixlinux.org:6969/announce,http://tracker2.artixlinux.org:6969/announce,http://torrents.artixlinux.org:6969/announce $f
#    mktorrent -a udp://tracker1.artixlinux.org:6969/announce,udp://tracker2.artixlinux.org:6969/announce,udp://torrents.artixlinux.org:6969/announce $f
    transmission-show -m $f.torrent >| $f.magnet
done

for torrent in *.torrent; do
  # add to transmission-daemon
    transmission-remote -a $torrent >/dev/null
done

if [ $CHANGED ]; then
    echo "Changes found, recreating hashes whitelist..."
    rm hashes_whitelist.new
    for torrent in *.torrent; do
      # update whitelist
        transmission-show -i $torrent | awk '/Hash/{print $NF}' >>hashes_whitelist.new
    done
    cat hashes_whitelist.new >| hashes_whitelist
fi

transmission-remote -t all -D -U --no-start-paused -SR -o -s
