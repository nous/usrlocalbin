:

source /usr/share/makepkg/util/message.sh
colorize

usage() {
    echo "Copy package to [universe], sign it and repo-add it."
    echo
    echo "Usage: $(basename $0) <packagename-ver-arch.pkg.tar.ext>"
    echo
}

repo="/srv/universe/x86_64"
[ $# -ne 1 ] && { usage; exit 1; }

[ -f $1 ] || { error "$1 not found"; exit 1; }

msg2 "Signing $1"
gpg  -u 98AE6E32AE0A4D126EACAC8C6023C3BEEAB08C4A -b $1
[ $? -eq 0 ] || { error "Failed to sign, aborting"; exit 1; }

msg2 "Copying $1 to $repo"
cp $1 $1.sig $repo/
[ $? -eq 0 ] || { error "Failed to copy to $repo, aborting"; exit 1; }

msg2 "Adding $1 to repo"
repo-add $repo/universe.db.tar.gz $repo/$1
[ $? -eq 0 ] || { error "Failed to copy to repo-add"; exit 1; }
