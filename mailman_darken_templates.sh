:

# Darken the templates

STYLE='<style type="text/css"> body {font-family: Ubuntu, Verdana, "Dejavu Sans", sans-serif;} </style>'
sed -i '/<[tT][iI][tT][lL][eE]>/a \ \ \ \ <style type="text\/css"> body {font-family: Ubuntu, Verdana, "Dejavu Sans", sans-serif; background: #1a1a1a; color: #d9d9d9;} input {background: #303030; color: #d9d9d9;} a:link {color: #7777ee;} a:visited {color: #ee77ee;} <\/style>' /usr/lib/mailman/templates/*/*.html
# Article must be monospace
sed -i '/Ubuntu/d' /usr/lib/mailman/templates/*/article.html
sed -i '/<\/[hH][eE][aA][dD]>/i \ \ \ \ <style type="text\/css"> body,pre {font-family: "DejaVu Sans Mono", Consolas, "Liberation Mono", Monaco, "Lucida Console", monospace; background: #1a1a1a; color: #d9d9d9;} a:link {color: #7777ee;} a:visited {color: #ee77ee;} <\/style>' /usr/lib/mailman/templates/*/article.html

ORIG='FONT COLOR="#000000"'
DEST='FONT COLOR="#DEDEDE"'
sed -i "s/$ORIG/$DEST/g" /usr/lib/mailman/templates/*/*.html

sed -i '/<BODY BGCOLOR="#ffffff">/a \ \ <FONT COLOR="#DEDEDE">' /usr/lib/mailman/templates/*/*.html
sed -i 's/<BODY BGCOLOR="#1A1A1A">/<BODY BGCOLOR="#1A1A1A" FONT TEXT="#DEDEDE">/gi' /usr/lib/mailman/templates/*/*.html
BGORIG='BGCOLOR="#ffffff"'
BGDEST='BGCOLOR="#1A1A1A"'
sed -i "s/$BGORIG/$BGDEST/gi" /usr/lib/mailman/templates/*/*.html
sed -i '/<BODY BGCOLOR="#1A1A1A">/a \ \ <FONT COLOR="#DEDEDE">' /usr/lib/mailman/templates/*/*.html

BGORIG='BGCOLOR="#cccccc"'
BGDEST='BGCOLOR="#4A4A4A"'
sed -i "s/$BGORIG/$BGDEST/gi" /usr/lib/mailman/templates/*/*.html

BGORIG='BGCOLOR="#fff0d0"'
BGDEST='BGCOLOR="#133353"'
sed -i "s/$BGORIG/$BGDEST/gi" /usr/lib/mailman/templates/*/*.html

BGORIG='BGCOLOR="#dddddd"'
BGDEST='BGCOLOR="#303030"'
sed -i "s/$BGORIG/$BGDEST/gi" /usr/lib/mailman/templates/*/*.html

BGORIG='BGCOLOR="#99CCFF"'
BGDEST='BGCOLOR="#003377"'
sed -i "s/$BGORIG/$BGDEST/gi" /usr/lib/mailman/templates/*/*.html

sed -i 's/<P>/<P><FONT COLOR="#DEDEDE">/gi' /usr/lib/mailman/templates/*/*.html
#sed -i '/<p>/a \ \ <FONT COLOR="#DEDEDE">' /usr/lib/mailman/templates/*/*.html

# Remove duplicates
sed -i '$!N; /^\(.*DEDEDE.*\)\n\1$/!P; D' /usr/lib/mailman/templates/*/*.html
sed -i '$!N; /^\(.*Ubuntu.*\)\n\1$/!P; D' /usr/lib/mailman/templates/*/*.html
sed -i '$!N; /^\(.*Consolas.*\)\n\1$/!P; D' /usr/lib/mailman/templates/*/*.html

#find /var/lib/mailman/archives/private/ -iname "*.html" -exec sed -i '/<\/[hH][eE][aA][dD]>/i \ \ \ \ <style type="text\/css"> body,pre {font-family: "DejaVu Sans Mono", Consolas, "Liberation Mono", Monaco, "Lucida Console", monospace; color: #d9d9d9;} a:link {color: #7777ee;} a:visited {color: #ee77ee;} <\/style>' {} +
#find /var/lib/mailman/archives/private/ -iname "*.html" -exec sed -i '$!N; /^\(.*Consolas.*\)\n\1$/!P; D' {} +
#find /var/lib/mailman/archives/private/ -iname "*.html" -exec sed -i '$!N; /^\(.*Ubuntu.*\)\n\1$/!P; D' {} +
