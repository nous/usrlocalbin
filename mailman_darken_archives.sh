:

# Darken the archives

STYLE='<style type="text/css"> body {font-family: Ubuntu, Verdana, "Dejavu Sans", sans-serif;} </style>'
find /var/lib/mailman/archives/private/ -iname "*.html" -exec sed -i '/<\/[hH][eE][aA][dD]>/i \ \ \ \ <style type="text\/css"> body,pre {font-family: "DejaVu Sans Mono", Consolas, "Liberation Mono", Monaco, "Lucida Console", monospace; color: #d9d9d9;} a:link {color: #7777ee;} a:visited {color: #ee77ee;} <\/style>' {} +
find /var/lib/mailman/archives/private/ -iname "*.html" -exec sed -i '/Ubuntu/d' {} +
#find /var/lib/mailman/archives/private/ -iname "*.html" -exec sed -i 's/<title><\/title>/<title><style type="text\/css"> body {font-family: "DejaVu Sans Mono", Consolas, "Liberation Mono", Monaco, "Lucida Console", monospace; color: #d9d9d9;} a:link {color: #7777ee;} a:visited {color: #ee77ee;} <\/style>' {} +


BGORIG='BGCOLOR="#ffffff"'
BGDEST='BGCOLOR="#1A1A1A"'
find /var/lib/mailman/archives/private/ -iname "*.html" -exec sed -i "s/$BGORIG/$BGDEST/gi" {} +

# Add font color value
find /var/lib/mailman/archives/private/ -iname "*.html" -exec sed -i '/<BODY BGCOLOR="#1A1A1A">/a \ \ <FONT COLOR="#DEDEDE">' {} +
# Remove duplicate lines
find /var/lib/mailman/archives/private/ -iname "*.html" -exec sed -i '$!N; /^\(.*DEDEDE.*\)\n\1$/!P; D' {} +
find /var/lib/mailman/archives/private/ -iname "*.html" -exec sed -i '$!N; /^\(.*Consolas.*\)\n\1$/!P; D' {} +
find /var/lib/mailman/archives/private/ -iname "*.html" -exec sed -i '$!N; /^\(.*Ubuntu.*\)\n\1$/!P; D' {} +
